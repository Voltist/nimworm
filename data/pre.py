import pandas as pd

edges = pd.read_csv("data/herm_full_edgelist.csv")

row_list = []
for index, row in edges.iterrows():
    if row["Source"].strip(" ").upper() == row["Source"].strip(" "):
        if row["Target"].strip(" ").upper() == row["Target"].strip(" "):
            row_list.append([row["Source"].strip(" "), row["Target"].strip(" "), row["Weight"], row["Type"]])

new = pd.DataFrame(row_list, columns=["source", "target", "weight", "type"])
new.to_csv("data/herm_full_edgelist.csv", index=False)