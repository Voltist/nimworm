# *C. elegans* Connectome Data

This is the connectome data used for nimWorm, retrieved from
[https://www.wormwiring.org/](https://www.wormwiring.org/) on `2020-02-18` and processed with `pre.py`.

`actions.csv` assigns each chemical connection either a 1 or a 0, for excitatory or inhibitory respectively. This was generated using neurotransmitter and receptor expression data from WormBase and CeNGEN. Due to imperfect data, this only accounts for approx. 60% of all chemical connections in the C. elegans connectome.