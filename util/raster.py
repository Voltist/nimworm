import json
import sys
import matplotlib.pyplot as plt

with open(sys.argv[1], "r") as fp:
    data = json.load(fp)

fig = plt.figure()
fig.subplots_adjust(hspace=0.4, wspace=0.4)
subplots = []
for index, run in enumerate(data["runs"].keys()):

    subplots.append(fig.add_subplot(2, 2, index+1))
    keys = data["runs"][run].keys()
    values = []
    for i in keys:
        values.append(data["runs"][run][i])

    yticks = [0]
    for i in range(len(keys)-1):
        yticks.append(1.5*(i+1))

    subplots[-1].eventplot([values[0], values[1]], lineoffsets=1.5)

    subplots[-1].set_title(run)
    subplots[-1].set_yticks(yticks)
    subplots[-1].set_yticklabels(keys)
    subplots[-1].set_xlabel("Timesteps")

plt.show()