# nimWorm

A simulation of the C. elegans worm written in Nim.

## Changelog

### 2020-01-28 | Worm V1

* Added worm object which stores the connectome structure, connectome state,
location, and rotation of the worm
* Added various procs that work with the worm object, most notably `updateConnectome`,
which advances the neural simulation by one step.

Currently the simulation uses a Leaky Integrate and Fire (LIF) model, with a
resting state of 0 and firing threshold of 30. Using positive integers for
neuron state is just for readability (subtract 70 for an approximation of mV).
Previous research indicates that single-variable threshold models like LIF are
90% accurate to the widely-used Hodgkin–Huxley-type models [1], which, combined
with their greater simplicity and computational efficiency, justifies their use.

### 2020-01-28 | Basic motor control

* Added generic `update` procedure that updates the connectome and does motor
control stuff.
* Added nose touch proc.

Here a novel approach is for 'motor control' that avoids the natural 'motors'
of C. elegans; muscles. Instead, the worm moves in a straight line until it's
rotation is changed (to a random direction within a range) when command
inter-neuron AVBR fires. The AVB neurons are known to correlate with sharp turns
associated with nose touch events [2], and is also postsynaptic to neurons known
to encode sharp turns during chemotaxis [3]. The turning range will be tweaked
later by studying live C. elegans.

The movement speed of the simulated worm is modulated by the activity in motor
neuron VB9, which is known to be part of a rhythm generating circuit fundamental
to C. elegans locomotion [4].

### 2020-01-29 | Chemotaxis scenario

* Added simple attractive chemotaxis simulation in `simulations/ac.nim`

This simulation demonstrates attractive/positive chemotaxis towards a gradient
peak. Currently there are only three states of stimulus to the ASE neurons
(which react asymmetrically to changes in salt concentration in a real worm [5]);
ASER stimulated more than ASEL when going down the gradient, ASEL stimulated
more than ASER when going up the gradient, or both equally stimulated when not
traversing the gradient. In a real C. elegans, the ratio of stimulation to the
ASE pair is proportional to the rate of gradient ascent/descent [5]. This will
be implemented later.

### 2020-02-01 | Feeding

* Added a simulation with a focus on pharyngeal neurons in
`simulation/feeding.nim` that recreates feeding behaviour consistent with
findings in real C. elegans.

The consumption of food (eg. bacteria) in C. elegans is achieved via two
behaviours; *pharyngeal pumping* and *isthmus peristalsis* [6]. The latter sucks
liquid and suspended bacteria into the lumen, sometimes followed soon after by
the former, which passes the contents into the intestine. Isthmus peristalsis
occurs roughly every four pumps. Both behaviours are regulated by serotonin
receptors on the motorneuron pair MC and motorneuron M4 [7], which is the
stimulation this simulation recreates by a constant depolarization of those
neurons. Pump duration is regulated by motorneuron/sensory neuron pair M3, which
sense muscle contraction in the pharynx [8]. This is simulated here by
depolarizing M3 when MC fires.

Overall, this simulation recreates the known feeding behaviour in C. elegans
very accurately, as indicated in the graphs it outputs. MC expresses 3-4
short increases in activity, followed by a sharp decrease in M4 activity,
repeated for the whole simulation.

### 2020-02-02 | Chemotaxis assay

* Added chemotaxis assay `simulations/assay.nim`
* Added a property to the Worm object to facilitate simulations with multiple
worms

This is an implementation of the Margie et al. (2013) [9] assay, which measures
the ability of a population of C. elegans to perform chemotaxis up a chemical
gradient. Anaesthetization is approximated here by 'freezing' a worm when it nears
a test or control point, which has the benefit of freeing up computational
resources.

Running this assay returns a score of `0.88`. More runs will be done once
the simulation is tweaked.

### 2020-03-21 | Updated and reliable connectome

* Changed to connectome from wormwiring
* Changed model parameters to reflect biological properties
* Tweaked model to support these changes

The old connectome was from the OpenWorm project and lacked clear sources. This new connectome is from Worm Wiring ([https://www.wormwiring.org](https://www.wormwiring.org)), which is run by academics at the *Albert Einstein College of Medicine Departments of Genetics and Neuroscience* and aims to maintain a high quality connectome compiled from cutting-edge research. The spiking threshold was also changed to be 15 to reflect the average range of -70 to 55 millivolts in a neuron during an action potential; this is mostly to avoid the need for conversion. In order to adapt the model to these two changes, multiple parameters were tuned until regular chemotaxis behaviour was observed.

### 2020-03-21 | Randomizing LIF order

This small change makes the order in which neurons are fired and propagate random each epoch. This makes the simulation behave more like a continuous system.

### 2020-03-29 | General model improvements

* Added data on connection dynamics
* Updated neuron-model relationships
* Retuned model

Integrated information on the dynamics of chemical connections (are they excitatory or inhibitory), based on data gathered from WormAtlas [10], WormBase and CeNGEN [11]. About 60% of all connections are now labelled, with the other 40% assumed to be excitatory.

The neurons that are used to trigger movement in the simulated environment have also been changed. Sharp turns are now triggered by action potentials in AIB; a neuron pair that's activity is known to be correlated with sharp turns [3]. AIBL seems to show strange over-active behaviour that makes it unsuitable for this purpose, so only AIBR is used. Further research is needed to understand if this has a biological basis or if it is a fault in the model/connectome. The neuron pair AVB, which has been shown in simulated models to modulate forward locomotion speed [12], now modulates forward locomotion speed in the physical simulation.

The model parameters have also been re-tuned to accommodate these changes.

### 2020-04-01 | Touch response

* Added a simulation script that demonstrates various touch responses
* Added a spike raster visualization script

This new script (`simulations/touch.nim`) exports spike occurrences over four short simulations; one for nose touch, one for anterior touch, another for posterior, and the one for 'plate tap'. The mechanosensory neurons that are stimulated during these simulations are in accordance with Goodman, M.B. [13]. The spike data is exported as JSON to `saved`, and can be visualized as spike rasters using `util/raster.py`. This visualization script may prove useful later on as well.

### 2020-04-01 | Retiring feeding simulation

* Removed feeding simulation

Unfortunately the feeding simulation has been removed because it is not compatable with the new model. A new version may arise later.

## References

1. Kistler, W. M., Gerstner, W., & Hemmen, J. L. van. (1997). *Reduction of the Hodgkin-Huxley Equations to a Single-Variable Threshold Model.* Neural Computation, 9(5), 1015–1045. doi:10.1162/neco.1997.9.5.1015
2. Chalfie, M., Sulston, J., White, J., Southgate, E., Thomson, J., & Brenner, S. (1985). *The neural circuit for touch sensitivity in Caenorhabditis elegans.* The Journal of Neuroscience, 5(4), 956–964. doi:10.1523/jneurosci.05-04-00956.1985
3. Luo, L., Wen, Q., Ren, J., Hendricks, M., Gershow, M., Qin, Y., … Zhang, Y. (2014). *Dynamic Encoding of Perception, Memory, and Movement in a C. elegans Chemotaxis Circuit.* Neuron, 82(5), 1115–1128. doi:10.1016/j.neuron.2014.05.010
4. Fouad, A. D., Teng, S., Mark, J. R., Liu, A., Alvarez-Illera, P., Ji, H., … Fang-Yen, C. (2018). *Distributed rhythm generators underlie Caenorhabditis elegans forward locomotion.* eLife, 7. doi:10.7554/elife.29913
5. Suzuki, H., Thiele, T. R., Faumont, S., Ezcurra, M., Lockery, S. R., & Schafer, W. R. (2008). *Functional asymmetry in Caenorhabditis elegans taste neurons and its computational role in chemotaxis.* Nature, 454(7200), 114–117. doi:10.1038/nature06927
6. Avery L, Thomas JH. *Feeding and Defecation.* In: Riddle DL, Blumenthal T, Meyer BJ, et al., editors. C. elegans II. 2nd edition. Cold Spring Harbor (NY): Cold Spring Harbor Laboratory Press; 1997. Chapter 24. Available from: [https://www.ncbi.nlm.nih.gov/books/NBK20138/](https://www.ncbi.nlm.nih.gov/books/NBK20138/)
7. Song, B. -m., & Avery, L. (2012). *Serotonin Activates Overall Feeding by Activating Two Separate Neural Pathways in Caenorhabditis elegans.* Journal of Neuroscience, 32(6), 1920–1931. doi:10.1523/jneurosci.2064-11.2012
8. Avery L. *Motor neuron M3 controls pharyngeal muscle relaxation timing in Caenorhabditis elegans.* J. Exp. Biol. 1993b;175:283–297.
9. Margie, O., Palmer, C., & Chin-Sang, I. (2013). *C. elegans Chemotaxis Assay.* Journal of Visualized Experiments, (74). doi:10.3791/50069
10. Loer, CM & Rand, JB (2016). *The Evidence for Classical Neurotransmitters in Caenorhabditis elegans*, in WormAtlas. doi:10.3908/wormatlas.5.200
11. Taylor S. R., Et al. (2019). *Expression profiling of the mature C. elegans nervous system by single-cell RNA-Sequencing.* https://doi.org/10.1101/737577
12. Bryden, J., & Cohen, N. (2008). *Neural control of Caenorhabditis elegans forward locomotion: the role of sensory feedback.* Biological Cybernetics, 98(4), 339–351. doi:10.1007/s00422-008-0212-6 
13. Goodman, M.B. *Mechanosensation* (January 06, 2006), WormBook, ed. The C. elegans Research Community, WormBook, doi/10.1895/wormbook.1.62.1, http://www.wormbook.org. 