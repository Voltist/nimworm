import parsecsv
import tables
import strutils
import random
import math

let ds = 1.0/100000.0

type
  # A nice alias for a point in 2D space
  Point* = array[2, float]

  # The worm type that contains all information about a single worm
  Worm* = object
    connectome*: Table[string, Table[string, seq[tuple[weight: int, action: int]]]]
    connectomeState*: Table[string, float]
    location*: Point
    rotation*: int
    speed*: float
    lastDist*: float # Optional value

# Loads the worm connectome into the Worm object
proc loadConnectome*(worm: var Worm, fn: string, actions: string) =

  # Init a csv parser and iterate over the rows in the data
  var p: CsvParser
  p.open(fn)
  p.readHeaderRow()
  while p.readRow():

    # Define these here to make the code more presentable
    let source = p.rowEntry("source")
    let target = p.rowEntry("target")
    let weight = parseInt(p.rowEntry("weight"))
    let typ = p.rowEntry("type")

    # Add link to connectome
    if worm.connectome.hasKey(source):
      worm.connectome[source][target] = @[(weight: weight, action: 1)]
    else:
      worm.connectome[source] = {target: @[(weight: weight, action: 1)]}.toTable

    # Bi-directional if gap junction
    if typ == "electrical":
      if worm.connectome.hasKey(target):
        worm.connectome[target][source] = @[(weight: weight, action: 1)]
      else:
        worm.connectome[target] = {source: @[(weight: weight, action: 1)]}.toTable

  p.close()

  p.open(actions)
  p.readHeaderRow()
  while p.readRow():

    let source = p.rowEntry("source")
    let target = p.rowEntry("target")
    let action = parseInt(p.rowEntry("e/i"))

    worm.connectome[source][target][0].action = action

  p.close()

  echo len(worm.connectome)

# Updates worm connectome (use as end of simulation step)
proc updateConnectome*(worm: var Worm): seq[string] =
  var fired: seq[string] = @[]

  # Copy connectome state for writing to
  var newState = worm.connectomeState

  # Leak
  for x in worm.connectome.keys():
    newState[x] *= 0.99
  
  var keys = newSeq[string](0)
  for i in worm.connectomeState.keys():
    keys.add(i)
  
  shuffle(keys)

  # Iterate over neurons and fire action potential if state greater than 30
  for x in keys:
    if worm.connectomeState[x] >= 15:
      fired.add(x)

      # Firing clears neuron state
      newState[x] = 0

      # Update state of all postsynaptic neurons according to weight
      for y in worm.connectome[x].keys():
        if worm.connectomeState.hasKey(y):
          for connection in worm.connectome[x][y]:
            if connection.action == 1:
              newState[y] += toFloat(connection.weight) * 0.15
            elif connection.action == 0:
              newState[y] -= toFloat(connection.weight) * 0.15

  # Copy newState into worm connectome
  worm.connectomeState = newState
  return fired

# Generic update proc for worm
proc update*(worm: var Worm, epoch: int): seq[string] =

  # Update the worm connectome
  let fired = worm.updateConnectome()

  # Sharp turn via AVB
  if "AIBR" in fired:
    worm.rotation += rand(90..270)

  # Set speed
  let tmp = worm.connectomeState["AVBL"] + worm.connectomeState["AVBR"]
  let s = 0.025 * (tmp/4.0)
  if worm.speed < s and s <= 0.025:
    worm.speed = s

  # Decay speed
  elif worm.speed >= ds:
      worm.speed -= ds

  # Update location
  worm.location[0] += sin(degToRad(toFloat(worm.rotation))) * worm.speed
  worm.location[1] -= cos(degToRad(toFloat(worm.rotation))) * worm.speed

  # Return seq of fired neurons in case the player wants to do anything with it
  return fired

# Initiates the worm connectome
proc initConnectome*(worm: var Worm) =
  for i in worm.connectome.keys():
    # Randomize starting values < firing threshold
    worm.connectomeState[i] = toFloat(rand(0..15))

  # Run for a bit with basic input to get going
  for i in 0..1000:
    worm.connectomeState["ASEL"] += 3.0
    worm.connectomeState["ASER"] += 3.0
    discard worm.updateConnectome()

# Initiates the worm softly
proc softInit*(worm: var Worm) =
  for i in worm.connectome.keys():
    worm.connectomeState[i] = 0.0

# Stimulates mechanosensory neurons in worm nose
proc noseTouch*(worm: var Worm) =
  worm.connectomeState["FLPL"] += 30.0
  worm.connectomeState["FLPL"] += 30.0
  worm.connectomeState["ASHL"] += 30.0
  worm.connectomeState["ASHR"] += 30.0
