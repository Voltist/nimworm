import worm as libWorm
import tables
import random
import math
import os
import strutils
import csfml

# Set random seed
randomize()

# Create a csfl window
var window = new_RenderWindow(
    videoMode_getDesktopMode(), "nimWorm",
    WindowStyle.Fullscreen
)
window.vertical_sync_enabled = true

proc dist(p1, p2: Point): float =
  return sqrt((p1[0] - p2[0]).pow(2) + (p1[1] - p2[1]).pow(2))

# Calculate values
let width = window.size.x
let height = window.size.y

let hw = toFloat(width) / 2
let hh = toFloat(height) / 2

let tx = cos(degToRad(45.0)) * (hh*0.9)
let ty = sin(degToRad(45.0)) * (hh*0.9)

let t1 = [hw-tx, hh-ty]
let t2 = [hw+tx, hh+ty]

let c1 = [hw+tx, hh-ty]
let c2 = [hw-tx, hh+ty]

# Create 50 worms
var worms: seq[Worm] = @[]
for i in 0..<50:
  var worm = Worm()
  worm.loadConnectome("data/herm_full_edgelist.csv", "data/actions.csv")
  worm.initConnectome()
  worm.rotation = rand(0..360)
  worm.speed = 0.025
  worm.location = [hw, hh]
  worms.add(worm)

# Frozen worms
var frozen: seq[Point] = @[]

# Stimulate chemosensory neurons by distance to nearest target
proc stim(worm: var Worm, point: Point): bool =
  let d = dist(worm.location, point)

  if d <= 50:
    return true

  if d <= worm.lastDist:
    worm.connectomeState["ASEL"] += 1.0
  elif d >= worm.lastDist:
    worm.connectomeState["ASER"] += 1.0
  else:
    worm.connectomeState["ASEL"] += 0.5
    worm.connectomeState["ASER"] += 0.5

  worm.lastDist = d
  return false

# Main loop
var epoch = 0
while window.open:

  epoch += 1

  # One hour or all worms anethetized
  if epoch == 360000 or len(frozen) == len(worms):
    break

  # Loop through queued events and process
  var event: Event
  while window.poll_event(event):
    case event.kind
      of EventType.Closed:
        window.close()
      else:
        discard

  # Loop through non-frozen worms
  for i in 0..<len(worms):
    let oldloc = worms[i].location
    if oldloc notin frozen:

      # Stimulate and see if ready to be frozen
      var freeze = false
      if worms[i].location[0] < hw and worms[i].location[1] < hh:
        if worms[i].stim(t1):
          freeze = true
      elif worms[i].location[0] > hw and worms[i].location[1] > hh:
        if worms[i].stim(t2):
          freeze = true

      # If not in target areas
      else:
        worms[i].connectomeState["ASEL"] += 3.0
        worms[i].connectomeState["ASER"] += 3.0

        if dist(worms[i].location, c1) <= 50 or
          dist(worms[i].location, c2) <= 50:
            freeze = true

      # Update worm
      let fired = worms[i].update(epoch)

      # Make sure worm stays in arena
      if dist(worms[i].location, [hw, hh]) >= hh:
        worms[i].location = oldloc
        worms[i].noseTouch()

      # Freeze worm if ready
      elif freeze:
        frozen.add(worms[i].location)

  # Every 64 simulation frames, draw
  if toFloat(epoch)/64 == round(toFloat(epoch)/64):
    window.clear Black

    var c = newCircleShape(radius=10)

    # Draw targets
    c.fillColor = Green
    c.position = vec2(t1[0]-5, t1[1]-5)
    window.draw c
    c.position = vec2(t2[0]-5, t2[1]-5)
    window.draw c

    # Draw controls
    c.fillColor = Red
    c.position = vec2(c1[0]-5, c1[1]-5)
    window.draw c
    c.position = vec2(c2[0]-5, c2[1]-5)
    window.draw c

    # Draw worms
    c.fillColor = White
    for i in worms:
      c.position = vec2(i.location[0]-5, i.location[1]-5)
      window.draw c

    # Draw border
    var border = newCircleShape(radius=hh)
    border.position = vec2(hw-hh, 0)
    border.outlineColor = White
    border.fillColor = Transparent
    border.outlineThickness = 10
    window.draw border

    window.display()

# Calculate score
var numTest = 0
var numControl = 0

for worm in worms:
  if worm.location[0] < hw and worm.location[1] < hh:
    numTest += 1
  elif worm.location[0] > hw and worm.location[1] > hh:
    numTest += 1

  elif dist(worm.location, [hw, hh]) > 50:
    numControl += 1

# Return score
echo toFloat(numTest - numControl) / toFloat(len(worms))
