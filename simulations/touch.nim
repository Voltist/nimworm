import worm as libWorm
import tables
import random

randomize()

let names = ["nose", "anterior", "posterior", "tap"]
let neurons = @[@["ASHL", "ASHR"], @["ALML", "ALMR"], @["PLML", "PLMR"], @["ASHL", "ASHR", "ALML", "ALMR", "PLML", "PLMR"]]

for index in 0..<len(names):

    # Create our worm
    var worm = Worm()
    worm.loadConnectome("data/herm_full_edgelist.csv", "data/actions.csv")
    worm.softInit()

    var forward = newSeq[int](0)
    var backward = newSeq[int](0)

    for epoch in 0..<30:

        if epoch < 15:
            for n in neurons[index]:
                worm.connectomeState[n] += 15

        # Progress the simulation one step forward
        let fired = worm.update(epoch)

        for f in fired:
            if f in ["AVBL", "AVBR", "PVCL", "PVCR"]:
                forward.add(epoch)
        
        for f in fired:
            if f in ["AVAL", "AVAR", "AVDL", "AVDR"]:
                backward.add(epoch)

    echo names[index] & ": "
    echo ("Forward:", len(forward))
    echo ("Backward:", len(backward))
    echo "--------------------------------"