import worm as libWorm
import tables
import random
import math
import os
import strutils
import csfml
import plotly

# Set random seed
randomize()

# Create a csfl window
var window = new_RenderWindow(
    videoMode_getDesktopMode(), "nimWorm",
    WindowStyle.Fullscreen
)
window.vertical_sync_enabled = true

# Create our worm
var worm = Worm()
worm.loadConnectome("data/herm_full_edgelist.csv", "data/actions.csv")
worm.initConnectome()
worm.rotation = 150
worm.speed = 0.025

# Various variables for keeping track of things
var ys: seq[float] = @[]
var xs: seq[float] = @[]
var lastdist = 9999.0
var history: seq[Point] = @[]

# Main loop
var epoch = 0
while window.open:

  epoch += 1
  xs.add(toFloat(epoch))

  # Loop through queued events and process
  var event: Event
  while window.poll_event(event):
    case event.kind
      of EventType.Closed:
        window.close()

      # Nosetouch at the press of the button T
      of EventType.KeyPressed:
        case event.key.code
          of KeyCode.T:
            worm.noseTouch()

          else:
            discard

      # You can click to set the worm position too
      of EventType.MouseButtonPressed:
        let position = mouse_getPosition()
        worm.location = [toFloat(position.x), toFloat(position.y)]

      else:
        discard

  # Store the state of AVBR for graphing later
  ys.add(worm.connectomeState["AVBL"] + worm.connectomeState["AVBR"])

  # Calculate the change in distance from gradient peak and apply appropriate stimulus
  let dist = sqrt((worm.location[0] - (toFloat(window.size.x)/2)).pow(2) + (worm.location[1] - (toFloat(window.size.y)/2)).pow(2))
  if abs(lastDist - dist) >= 0.01:
    if dist > lastDist:
      worm.connectomeState["ASER"] += 1.0
    elif dist < lastDist:
      worm.connectomeState["ASEL"] += 1.0
  else:
    worm.connectomeState["ASEL"] += 0.5
    worm.connectomeState["ASER"] += 0.5

  # For comparison later
  lastDist = dist

  # If the worm touches the edge, dont let it go any further and do a nosetouch
  if worm.location[0] < 0:
    worm.location[0] = 0
    worm.noseTouch()
  elif worm.location[0] > toFloat(window.size.x):
    worm.location[0] = toFloat(window.size.x)
    worm.noseTouch()
  if worm.location[1] < 0:
    worm.location[1] = 0
    worm.noseTouch()
  elif worm.location[1] > toFloat(window.size.y):
    worm.location[1] = toFloat(window.size.y)
    worm.noseTouch()

  # Progress the simulation one step forward
  let fired = worm.update(epoch)

  # Every 64 simulation frames, draw
  if toFloat(epoch)/64 == round(toFloat(epoch)/64):
    window.clear Black
    history.add(worm.location)

    var target = newCircleShape(radius=10)
    target.fillColor = Green
    target.position = vec2((toFloat(window.size.x)/2.0)-5, (toFloat(window.size.y)/2.0)-5)
    window.draw target

    var h = newCircleShape(radius=1)
    h.fillColor = Red
    for i in history:
      h.position = vec2(i[0], i[1])
      window.draw h

    var c = newCircleShape(radius=10)
    c.fillColor = White
    c.position = vec2(worm.location[0]-5, worm.location[1]-5)
    window.draw c

    window.display()

# Graph the activity of AVBR
var ava = Trace[float](mode: PlotMode.Lines, name:"AVA")
ava.xs = xs
ava.ys = ys

var layout = Layout(title: "AVBR", width: window.size.x, height: toInt(window.size.y/2),
                    xaxis: Axis(title:"Epochs"),
                    yaxis:Axis(title: "Activation"), autosize:true)

var p = Plot[float](layout:layout, traces: @[ava])
p.show()
